/**
 * Created by icastilho on 4/9/16.
 */
angular.module('powervote.directives.formname', [])
.directive('formname', function($q, $timeout, FormService) {
   return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {

         ctrl.$asyncValidators.formname = function(modelValue, viewValue) {

            if (ctrl.$isEmpty(modelValue)) {
               // consider empty model valid
               return $q.when();
            }

            var def = $q.defer();

            FormService.find('form_name',modelValue).then(function(result){
               // Mock a delayed response
               console.log('directive forme_name ', result);
               if(result.data.length>0){
                  console.log('rejected');
                  def.reject();
               }else{
                  console.log('valid');
                  def.resolve();
               }

            });

            return def.promise;
         };
      }
   };
});