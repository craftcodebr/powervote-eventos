/**
 * Created by icastilho on 14/06/15.
 */
'use strict';
angular.module('powervote.controller.upload', [])
.controller( 'FileUploadCtrl', function FileUploadCtrl( $scope,FileUploader) {

   $scope.notificacoes = [];

   var uploader = $scope.uploader = new FileUploader({
      url: 'http://localhost:1337/file/upload',
      alias: 'background',
      queueLimit: 2
      //formData:[{id:'55746738b23044541f17547b'}]
   });

   // FILTERS
   uploader.filters.push({
      name: 'imageFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
         var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
         return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
   });
   uploader.filters.push({
      name: 'shiftFilter',
      fn: function(item, options) {
         console.log(uploader.queue);
         if(uploader.queue.length == 1){
            uploader.clearQueue();
         }
         return true;
      }
   });

   uploader.onBeforeUploadItem = function(fileItem){
     console.log({item: fileItem.file.name});
   }
   uploader.onCompleteItem = function(fileItem, response, status, headers){
      $scope.notificacoes.push({item: fileItem.file.name});
   }

});

