/**
 * Created by icastilho on 08/06/15.
 */
'use strict';
angular.module('powervote.controller.evento', [])
.controller('EventoCtrl',  function ( $scope,$state,FormService, Event) {

   $scope.eventos = Event.query();
   $scope.pageSize = 5;
   $scope.delete = function(evento) {
         evento.$delete(function() {
            console.info('Evento deletado!!!');
            $scope.eventos = Event.query();
            console.info('Evento deletado')
         });
   };

      $scope.goToForm = function(evento){
         console.log('gotoForm',evento);
         $state.go('evento.forms',{'evento':evento});
      };


      $scope.edit = function (evento) {
      console.log('edit...', evento);
      $state.go('^.edit', {'evento':evento});
   };

})
   .controller('CreateEventoCtrl', function CreateEventoController( $scope, $state, FormService, FileUploader, Event,ENV) {

      /** Action Prepare Screen */
      console.info('Create New Event');
      $scope.evento = new Event();
      $scope.evento.textcollor = '#FFFFFF';
      $scope.evento.backgroundcollor = '#000000';

      var logo = $scope.logo = createUploader(FileUploader, ENV);

      logo.onBeforeUploadItem = function(fileItem){
            console.trace('beforeUplogo',{item: fileItem.file.name});
            fileItem.formData.push({'fileType': 'logo'});
            fileItem.formData.push({'id': $scope.evento.id});
            console.log(fileItem);
         };

      var background = $scope.background = createUploader(FileUploader, ENV);

      background.onBeforeUploadItem = function(fileItem){
         console.trace('beforeUpbackground',{item: fileItem.file.name});
         fileItem.formData.push({'fileType': 'background'});
         fileItem.formData.push({'id': $scope.evento.id});
         console.log(fileItem);
      };

      /** Controller Actions */

      $scope.init = initColorPicker();

      $scope.submit = function () {
         console.info('Submit Evento');

         if($scope.evento.id){
            FormService.update($scope.evento);
         }else {

            FormService.save($scope.evento)
               .success(function (data) {
                  console.trace('Create eventos  SUCCESS'.green);
                  $scope.evento = data;
                  console.log($scope.evento);

                  if (background.queue.length > 0) {
                     background.uploadAll();
                  }
                  if (logo.queue.length > 0) {
                     logo.uploadAll();
                  }

                  $state.go("^.list", {'evento': $scope.evento});
               }).error(function (err) {
                  console.error(err);
               });
         }
      };
   })

   .controller('EditEventoCtrl', function EditEventoController( $scope, $state, $stateParams, $q, FormService, FileUploader, Event, ENV) {

      if(!$stateParams.evento){
         $scope.logo = $scope.background =  createUploader(FileUploader, ENV);
         $state.go('^.list');
         return;
      }else{
         console.trace('EditEventoCtrl: ', $stateParams.evento.id);
         $scope.evento = $stateParams.evento;

         $scope.evento = Event.get({id: $scope.evento.id}, function () {
            console.log('>>', $scope.evento.logoUrl);
         });

         var logo = $scope.logo = createUploader(FileUploader, ENV);

         logo.onBeforeUploadItem = function (fileItem) {
            console.trace('beforeUplogo', {item: fileItem.file.name});
            fileItem.formData.push({'fileType': 'logo'});
            fileItem.formData.push({'id': $scope.evento.id});
         };

         var background = $scope.background = createUploader(FileUploader, ENV);

         background.onBeforeUploadItem = function (fileItem) {
            console.trace('beforeUpbackground', {item: fileItem.file.name});
            fileItem.formData.push({'fileType': 'background'});
            fileItem.formData.push({'id': $scope.evento.id});
         };

         /** Controller Actions */
         $scope.init = initColorPicker();

         $scope.submit = function () {

            update()
               .then(function () {
                  console.log('Enviando logo...');
                  logo.uploadAll();
                  return logoPromise;
               })
               .then(function () {
                  console.log('Enviando background...');
                  background.uploadAll();
                  return backgroundPromise;
               })
               .catch(function (fallback) {
                  console.error(fallback);
               }).finally(function () {
                  console.log("Uploads finished");
                  $state.go('^.list');
               });
         }

         var logoPromise = function () {
            var deferredLogo = $q.defer();
            logo.onCompleteItem = function (item, response, status, headers) {
               console.log('Complete Item status:', status);
               deferredLogo.resolve;
            };
            return deferredLogo.promise;
         }
         var backgroundPromise = function () {
            var deferredBackground = $q.defer();
            background.onCompleteItem = function (item, response, status, headers) {
               console.log('Complete Item status:', status);
               deferredBackground.resolve;
            };
            return deferredBackground.promise;
         }

         var update = function () {
            var deferred = $q.defer();
            $scope.evento.$update(function () {
               console.info('Evento ', $scope.evento.name, ' salvo com Sucesso!!!');
               deferred.resolve();
            });
            return deferred.promise;
         };
      }
   });

function createUploader(FileUploader, ENV){
   /** Initiate Uploade */
   var uploader  = new FileUploader({
      url: ENV.apiEndpoint+'/file/upload',
      queueLimit: 2
   });
   // FILTERS
   uploader.filters.push({
      name: 'imageFilter',
      fn: function imageFilter(item, options) {
         var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
         return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
   });
   uploader.filters.push({
      name: 'shiftFilter',
      fn: function shiftFilter(item, options) {
         if(uploader.queue.length == 1){
            uploader.clearQueue();
         }
         return true;
      }
   });
   uploader.onCompleteAll = function(){
      console.trace('Complete All');
   };

   return uploader;
}

function initColorPicker() {
   //Color Picker
   $('.colorpicker-texto').colorpicker({color:'#FFFFFF'}).on('changeColor.colorpicker', function (event) {
      $scope.evento.textcollor = event.color.toHex();
   });
   $('.colorpicker-background').colorpicker().on('changeColor.colorpicker', function (event) {
      $scope.evento.backgroundcollor = event.color.toHex();
   });
}