'use strict';
var angularApp = angular.module('powervote', [
   'config',
   'ngRoute',
   'ngResource',
   'ngAnimate',
   'ui.router',
   'ui.bootstrap',
   'angularFileUpload',
   'angular-loading-bar',
   '$strap.directives',
   'angularUtils.directives.dirPagination',
   'powervote.controller.header',
   'powervote.controller.atividade',
   'powervote.controller.evento',
   'powervote.controller.forms.view',
   'powervote.controller.upload',
   'powervote.services.event',
   'powervote.services.form',
   'powervote.services.formdata',
   'powervote.directives.field',
   'powervote.directives.form',
   'powervote.directives.formname',
   'ngCep'
])
   .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
      cfpLoadingBarProvider.latencyThreshold = 500;
   }])
   .config(['$stateProvider', '$urlRouterProvider',
      function ($stateProvider , $urlRouterProvider) {
         $urlRouterProvider.otherwise('/powervote/home');
         $stateProvider
            .state('powervote', {
               url: '/powervote',
               abstract: true,
               views: {
                  admin: {
                     templateUrl: 'views/template/admin.html',
                  }
               }
            })
            .state('powervote.home', {
               url: '/home',
               templateUrl: 'views/main.html',
            })

//            Não remover
/*            .state('evento.list', {
               url:"/list",
               templateUrl: 'views/evento/evento.html',
               controller: 'EventoCtrl'
            })
            .state('evento.create', {
               url:"/create",
               templateUrl: 'views/evento/create.html',
               controller: 'CreateEventoCtrl'
            })
            .state('evento.edit', {
               url:"/edit",
               templateUrl: 'views/evento/create.html',
               controller: 'EditEventoCtrl',
               params: {evento: null}
            })*/
            .state('powervote.list', {
               url:"/forms/list",
               templateUrl: 'views/form/forms.html',
               controller: 'FormsCtrl'
            })
            .state('powervote.create', {
               url: '/forms/create',
               templateUrl: 'views/form/create.html',
               controller: 'CreateCtrl'
            })
            .state('powervote.view', {
               url:"/forms/view/:id",
               templateUrl: 'views/form/view.html',
               controller: 'ViewCtrl'
            })
            .state('powervote.edit', {
               url:"/forms/edit/:id",
               templateUrl: 'views/form/create.html',
               controller: 'CreateCtrl'
            })
            .state('powervote.answer', {
               url:"/forms/:name",
               templateUrl: 'views/form/answer.html',
               controller: 'AnswerCtrl'
            })
            .state('powervote.upload', {
               url: '/upload',
               templateUrl: 'views/upload.html',
               controller: 'FileUploadCtrl'
            });

      }]).run(['$rootScope',  function() {}]);

