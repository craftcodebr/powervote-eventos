/**
 * Created by icastilho on 15/06/15.
 */
angular.module('powervote.services.event', ['config','ngResource']).factory('Event', function($resource, ENV) {
   //return $resource('http://localhost:1337/event/:id',{id:'@id'}, {
   return $resource(ENV.apiEndpoint+'/event/:id',{id:'@id'}, {
      update: {
         method: 'PUT'
      }
   });
});
angularApp.config(['$resourceProvider', function($resourceProvider) {
   $resourceProvider.defaults.stripTrailingSlashes = false;
}]);