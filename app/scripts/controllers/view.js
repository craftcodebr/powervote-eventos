'use strict';
angular.module('powervote.controller.forms.view', [])
   .controller('ViewCtrl', function ($scope, FormDataService, $stateParams) {
      $scope.form = {};
      // read form with given id
      console.log('ViewCtrl', $stateParams.id);

      FormDataService.find('form', $stateParams.id).then(function(result) {
         $scope.datas = result.data;
      });
   })

   .controller('FormsCtrl',  function ( $scope,$state,FormService) {
      $scope.forms = [];
      FormService.list().then(function(result){
         $scope.forms = result.data;
      });

      $scope.pageSize = 5;
      $scope.delete = function(form) {
         form.$delete(function() {
            console.info('Form deletado!!!');
            $scope.forms = FormService.list();
            console.info('Form deletado')
         });
      };

      $scope.goToForm = function(form){
         $state.go('form.forms',{'form':form});
      };

      $scope.edit = function (form) {
         $state.go('^.edit/', {'id':form.id});
      };

   })
   .controller('AnswerCtrl', function ($scope, $stateParams, FormService, FormDataService) {
      $scope.previewForm = {
         canRecord: true
      };

         // read form with given id
      console.log('form_name', $stateParams.name);

      FormService.find('form_name', $stateParams.name)
         .then(function(result) {
            $scope.previewForm = result.data[0];
            return FormDataService.count($scope.previewForm.id);
         })
         .then(function(result){
            console.log(result);
            console.log('haslimit:' + $scope.previewForm.haslimit + ' records:' + $scope.records + ' limit:'+$scope.previewForm.record_limit);
            if($scope.previewForm.haslimit) {
               $scope.previewForm.canRecord = ($scope.previewForm.haslimit && ($scope.records < $scope.previewForm.record_limit));
            }else{
               $scope.previewForm.canRecord = true;
            }
            console.log($scope.previewForm.canRecord)
         });
   });