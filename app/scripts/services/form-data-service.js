/**
 * Created by icastilho on 4/9/16.
 */
'use strict';
angular.module('powervote.services.formdata', [])
   .service('FormDataService', function FormDataService($http, ENV) {
      var model = "/formdata";
      return {
         list:function(){
            return $http.get(ENV.apiEndpoint+model);
         },
         save:function(data){
            return $http.post(ENV.apiEndpoint+model+'/create', data);
         },
         delete:function(id){
            return $http.delete(ENV.apiEndpoint+model+'/'+id);
         },
         get:function(id){
            return $http.get(ENV.apiEndpoint+model+'/'+id);
         },
         find:function(key,value){
            return $http.get(ENV.apiEndpoint+model+'?'+key+'='+value);
         },
         count:function(id){
            return $http.get(ENV.apiEndpoint+model+'/count?formId='+id);
         },
         update:function(data){
            return $http.put(ENV.apiEndpoint+model+'/'+data.id,data);
         }
      }
   });