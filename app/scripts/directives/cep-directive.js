/**
 * Created by icastilho on 4/17/16.
 */
'use strict';


angular
   .module('ngCep', ['angular.viacep'])
   .directive('cepDirective', function() {

      var linker = function(scope, element) {



      };

      return {
         controller: function ($scope) {
            $scope.field.field_address = {
               street: null,
               number: null,
               adjunct: null,
               neighborhood: null,
               uf: null,
               city: null
            }
         },
         templateUrl: './views/directive-templates/address.html',
         restrict: 'E',
         scope: {
            field: '=',
            form: '='
         },
         link: linker
      }
   });