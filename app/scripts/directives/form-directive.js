'use strict';

angular.module('powervote.directives.form', [])
   .directive('formDirective', function (FormDataService, $state) {
      return {
         controller: function($scope){

            $scope.submit = function() {
               console.log('submit form:',$scope.form);
               $scope.form.submitted = true;
               if ($scope.form.id) {
                  var data = {
                     form: $scope.form.id,
                     fields: $scope.form.form_fields
                  };
                  FormDataService.save(data);
               }
            };

            $scope.cancel = function(){
               if($scope.form.id)
                  $state.go('^.list');
               else
                  alert('Form canceled..');
            }
         },
         templateUrl: './views/directive-templates/form/form.html',
         restrict: 'E',
         scope: {
            form:'='
         }
      };
   });
