/**
 * Created by icastilho on 17/06/15.
 */
var express = require('express');
var app = express();
app.use(express.static(__dirname + '/dist'));
app.listen(process.env.PORT || 3000);