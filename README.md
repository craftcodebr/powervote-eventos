powervote
======================


## About

### Dependencies

* AngularJS 1.0.8+
* Bootstrap 2.3.2+
* Grunt (to build the project) 0.4.1+

## Installing the Application

* First, install bower and grunt globally if you don't have already:
 ```
  $ npm install -g grunt-cli
  $ npm install -g grunt
  $ npm install -g bower 
 ```

* Then, clone git repo and cd to project folder: 
 ``` 
  $ git clone https://icastilho@bitbucket.org/craftcodebr/powervote-eventos.git
  $ cd powervote-eventos/ 
 ```
  
* Now, you need to install required node packages: 
 ``` 
  $ npm install 
 ``` 
 
* And finally, install bower dependencies: 
 ``` 
  $ bower install 
 ``` 
 
## Launching the App

* Start the grunt server: 
 ```
  $ grunt server --force
 ```
 
* It will open the AngularJS app in your default browser. 

