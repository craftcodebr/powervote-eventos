'use strict';
angular.module('powervote.services.form', [])
   .service('FormService', function FormService($http, ENV) {
      var model = "/form";
      return {
         fields:[
            {
               name : 'textfield',
               value : 'Textfield'
            },
            {
               name : 'email',
               value : 'E-mail'
            },
            {
               name : 'password',
               value : 'Password'
            },
            {
               name : 'radio',
               value : 'Radio Buttons'
            },
            {
               name : 'dropdown',
               value : 'Dropdown List'
            },
            {
               name : 'date',
               value : 'Date'
            },
            {
               name : 'textarea',
               value : 'Text Area'
            },
            {
               name : 'checkbox',
               value : 'Checkbox'
            },
            {
               name : 'hidden',
               value : 'Hidden'
            },
            {
               name : 'cpf',
               value : 'CPF'
            },
            {
               name : 'cnpj',
               value : 'CNPJ'
            },
            {
               name : 'cep',
               value : 'CEP'
            },
            {
               name : 'phonenumber',
               value : 'Phone Number'
            }
         ],
         list:function(){
            return $http.get(ENV.apiEndpoint+model);
         },
         save:function(form){
            return $http.post(ENV.apiEndpoint+model+'/create', form);
         },
         delete:function(id){
            return $http.delete(ENV.apiEndpoint+model+'/'+id);
         },
         get:function(id){
            return $http.get(ENV.apiEndpoint+model+'/'+id);
         },
         find:function(key,value){
            return $http.get(ENV.apiEndpoint+model+'?'+key+'='+value);
         },
         update:function(form){
            return $http.put(ENV.apiEndpoint+model+'/'+form.id,form);
         }

      };
   });
